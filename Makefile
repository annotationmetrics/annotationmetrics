all: knitr bib tex
	pdflatex Paper_Annotated_Code-Guerra.tex
	evince Paper_Annotated_Code-Guerra.pdf &
	open -a Adobe\ Acrobat\ Reader\ DC.app Paper_Annotated_Code-Guerra.pdf 

bib: Paper_Annotated_Code-Guerra.bib
	pdflatex Paper_Annotated_Code-Guerra.tex
	bibtex Paper_Annotated_Code-Guerra

tex: Paper_Annotated_Code-Guerra.tex
	pdflatex Paper_Annotated_Code-Guerra.tex

clean:
	rm -f *.out *.spl *.bbl *.aux *.blg *.log 

knitr:
	Rscript Paper_Annotated_Code-Guerra.R
	mv 7-dataAnalysis.tex sections/7-dataAnalysis.tex
