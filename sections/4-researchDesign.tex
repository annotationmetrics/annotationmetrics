\section{Research Design}                                             
\label{sec:ResearchDesign}

In this paper, we argue that previous works studied annotated code from a different perspective due to the lack of approaches to assess the characteristics of code annotations. Thus, this section presents how this research was designed, describing the steps we adopted to reach our main goal:  \textit{assess the usage of annotated code from a software developer point of view}.

\subsection{Research Questions}

Based on such goal, we focus our study in the following research questions:

\begin{description}

\item [\#RQ1] - What measurements could be performed in the source code to assess the characteristics of its code annotations usage?

Code annotations and annotated code elements have several characteristics that can be measured. For instance, an annotation has attributes values that can be defined. Code elements, such as classes and methods, can receive several annotations. To answer this question, we identify essential characteristics and propose a suite of candidate metrics that enables the assessment of them. Moreover, to provide a means to generate and interpret the proposed metrics, we will use the Goal-Question-Metric (GQM) approach\citep{basili1992,basili1994}.

\item [\#RQ2] - For each metric, is it possible to define reference thresholds that can be used to classify its values?

Annotation metrics might behave as object-oriented metrics, that in general might not have a meaningful average value. Our candidate metrics will be extracted from the source code of a set of selected projects, and statistical analyses of the values distribution will be performed on them. To provide a first step in interpreting the metrics, an analysis based on percentile rank (our proposed approach) will also be carried out. Considering that annotations metrics might behave according to an exponential graph, to answer this question, we also perform empirical analyses based on our proposed percentile rank approach and Lanza's method (based on average and standard deviation values). Afterwards, we determine which approach is the most suitable for thresholds values calculation.

\item [\#RQ3] - What is the common profile of a class that uses code annotations in Java? How large are the metrics outliers found?

The values obtained for each metric, their distribution and the code inspection of outliers can help to reach conclusions about how annotations are used by the projects, and how the candidate metrics can be used to detect potential design problems. It is important to detect if there are very large outliers because they can become a problem in code maintenance. Detecting common profiles in annotated classes may help developers keep track of code quality. 

\item [\#RQ4] - May the usage of annotations create problems that can compromise code maintenance?

Currently, the software engineering community lacks studies dedicated to understanding how code annotations can impact software maintenance. Having threshold values available as a tool to analyze annotations, and being able to point out the presence of outlier values will indicate if code annotations can indeed become troublesome in maintaing the code. This question will leave a lot of ground to be researched in future studies.

\end{description}


\subsection{Research Method}
\label{sec:ResearchMethod}

This section describes the steps performed to answer the research questions. 

\begin{description}

\item [Step 1] - Propose the candidate metrics

Based on important characteristics of annotations and annotated elements, as well as, using the GQM paradigm\citep{basili1992,basili1994}, we propose a metrics suite composed by the candidate metrics presented in Section \ref{sec:AnnotatedCodesAssessment}. 

\item [Step 2] - Create a tool for the candidate metrics extraction

To enable the candidate metrics extraction in projects, we developed a tool named Annotation Sniffer (ASniffer) that analyze Java source code and collect information about annotations. The tool generates an XML report containing the metrics for all project classes, annotations, and code elements~\footnote{\url{https://gitlab.com/annotationmetrics/annotationsniffer}}.

\item [Step 3] - Select projects to be analyzed

To enable the analysis of metric values in real-world projects, a set of open source projects were selected. We chose projects that have a different number of annotated classes. For instance, some projects contain 80\% of their classes annotated, while others contain only 10\% of annotated classes. The selected projects are listed in Section \ref{sec:dataCollection}.


\item [Step 4] - Data extraction and processing

The metrics values were extracted from the selected 25 projects, providing 24,947 annotated Java classes as our actual sample data. The collected values were submitted to a Python script\footnote{\url{https://gitlab.com/annotationmetrics/annotationmetrics/blob/master/data/output_FromXMLtoCSV/glueMetrics.py}} to prepare the data. Afterwards, an R script\footnote{\url{https://gitlab.com/annotationmetrics/annotationmetrics/tree/master/data/R}} performed the statistical calculations on the prepared data. A distribution graph was generated for every metric of each project.

\item [Step 5] - Statistical and qualitative analysis

It was analyzed the distribution of the candidate metrics values among all real-world projects and verified if the average value was meaningful. When the normal distribution was not able to fit the data, a percentile rank analysis (empirical) was also performed. In short, we compared normal distribution (Lanza's approach) and empirical percentile rank (the proposed approach). Based on the data and code inspection on outliers, a qualitative analysis was performed to identify how the metrics can characterize a common usage of annotations and how design problems can be detected.

\end{description}

\subsubsection{Data Collection}
\label{sec:dataCollection}

The collected data came from a total of 24,947 annotated Java classes extracted from 25 real-world projects selected to participate in this analysis. Given that annotations are an optional language feature, projects were elected considering the presence of annotations and the projects domains.

\begin{table}[hbt]
\centering
\caption{Selected Projects.}
\label{tab:selectedProjects}
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|l|l|r|r|}
\hline

\textbf{Project}	& \textbf{Repository}				& \textbf{Version} 		\\ \hline

Agilefant 		& \url{github.com/Agilefant/agilefant}          & 3.5.4            		\\ \hline
ANTLR 			& \url{github.com/antlr/antlr4}              	& 4.5.3            		\\ \hline
Apache Derby 		& \url{github.com/apache/derby}       		& 10.12.1.1        		\\ \hline
Apache Isis        	& \url{github.com/apache/isis}			& 1.13             		\\ \hline
Apache Tapestry 	& \url{github.com/apache/tapestry-5}    	& 5.4.1     	        	\\ \hline
Apache Tomcat 		& \url{github.com/apache/tomcat}      		& 9.0.0            		\\ \hline
ArgoUML 		& \url{argouml.tigris.org/source/browse/argouml/trunk/src}   & 0.34     	\\ \hline
Eclipse CheckStyle 	& \url{github.com/acanda/eclipse-cs}       & 6.2.0    			\\ \hline
Dependometer 		& \url{github.com/dheraclio/dependometer}       & 1.2.9            		\\ \hline
ElasticSearch 		& \url{github.com/elastic/elasticsearch}      	& 5.0.0-rc1        		\\ \hline
Hibernate Commons 	& \url{github.com/hibernate/hibernate-commons-annotations}   & 4.0.5		\\ \hline
Hibernate Core 		& \url{github.com/hibernate/hibernate-orm}     	& 5.2.0            		\\ \hline
JChemPaint 		& \url{github.com/JChemPaint/jchempaint}        & 3.3-1210         		\\ \hline
Jenkins 		& \url{github.com/jenkinsci/jenkins}            & 2.25             		\\ \hline
JGit 			& \url{github.com/eclipse/jgit}               	& 4.5.0            		\\ \hline
JMock 			& \url{github.com/jmock-developers/jmock-library}            & 2.8.2    	\\ \hline
JUnit 			& \url{github.com/junit-team/junit5}            & 5.0.0-M2         		\\ \hline
Lombok 			& \url{github.com/rzwitserloot/lombok} 		& 1.16.10          		\\ \hline
Megamek 		& \url{github.com/MegaMek/megamek}            	& 0.41.24          		\\ \hline
MetricMiner  		& \url{github.com/mauricioaniche/metricminer2}  & 2.6              		\\ \hline
OpenCMS 		& \url{github.com/alkacon/opencms-core}         & 10.0.1           		\\ \hline
OVal 			& \url{github.com/sebthom/oval}               	& 1.86         			\\ \hline
Spring Integration 	& \url{github.com/spring-projects/spring-integration}        & 4.3.4    	\\ \hline
VRaptor 		& \url{github.com/caelum/vraptor4}      	& 4.2.0-RC4	        	\\ \hline
VoltDB 			& \url{github.com/VoltDB/voltdb}             	& 6.5.1            		\\ \hline


\end{tabular}
\end{adjustbox}
\end{table}

To conduct our analysis, we needed a large set of annotated classes as our sample. Selecting these 25 projects were not straightforward. Using random selection was not an option, since we needed projects that contained enough annotated classes to provide meaningful data to us. For instance, selecting the top-rated projects on GitHub\footnote{By top GitHub projects, we mean the projects rated with top stars} did not provide the sample we expected, since not all of these top projects had enough annotated classes. Projects such as Hibernate, JUnit, and Apache TomCat are not listed in the top GitHub Java projects\footnote{\url{https://gitlab.com/annotationmetrics/annotationmetrics/blob/master/topStarsJavaProjects.txt}}, but they are well known Java projects and are recognized for their high annotation usage. Therefore, they were fundamental in our analysis, in particular to discuss the existing outliers. The selected projects are shown in Table \ref{tab:selectedProjects}.

\begin{table}[hbt]
\centering
\caption{Project Dimensions}
\label{tab:projectDimensions}
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|l|r|r|r|r|r|}
\hline
\multicolumn{1}{|c|}{\textbf{Project}} & \multicolumn{1}{c|}{\textbf{Type}} & \multicolumn{1}{c|}{\textbf{PAC (\%)}} & \multicolumn{1}{l|}{\textbf{PAC Category}} & \textbf{LOC} & \multicolumn{1}{l|}{\textbf{LOC Category}} \\ \hline
Agilefant                              & Application                        & 10.50                                                & Low Usage                                                    & 43,539        & Low                                    \\ \hline
Dependometer                           & Application                        & 35.00                                                & Low Usage                                                    & 28,123        & Low                                          \\ \hline
ANTLR                                  & Application                        & 10.60                                                & Low Usage                                                    & 101,600       & High                                          \\ \hline
ArgoUML                                & Application                        & 31.70                                                & Low Usage                                                    & 195,670       & High                                          \\ \hline
Apache Derby                           & Application                        & 18.80                                                & Low Usage                                                     & 689,869       & High                                          \\ \hline
Eclipse Checkstyle                     & Application                        & 44.70                                                & Medium Usage                                                    & 20,453        & Low                                          \\ \hline
JChemPaint                             & Application                        & 63.70                                                & Medium Usage                                                    & 27,371        & Low                                          \\ \hline
Jenkins                                & Application                        & 64.00                                                & Medium Usage                                                    & 124,576       & High                                         \\ \hline
Elastic Search                         & Application                        & 48.20                                                & Medium Usage                                                    & 615,637       & High                                         \\ \hline
Megamek                                & Application                        & 70.60                                                & High Usage                                                    & 306,210       & High                                          \\ \hline
OpenCMS                                & Application                        & 72.60                                                & High Usage                                                    & 476,074       & High                                          \\ \hline
VoltDB                                 & Application                        & 80.80                                                & High Usage                                                    & 542,030       & High                                          \\ \hline
Apache Isis                            & Framework                          & 21.80                                                & Low Usage                                                    & 163,665       & High                                          \\ \hline
Apache Tapestry                        & Framework                          & 25.60                                                & Low Usage                                                    & 156,450       & High                                          \\ \hline
Apache Tomcat                          & Framework                          & 31.00                                                & Low Usage                                                    & 300,819       & High                                          \\ \hline
Hibernate Commons                      & Framework                          & 54.40                                                & Medium Usage                                                    & 2,812         & Low                                          \\ \hline
JGit                                   & Framework                          & 64.80                                                & Medium Usage                                                    & 173,681       & High                                         \\ \hline
Hibernate Core                         & Framework                          & 54.70                                                & Medium Usage                                                    & 593,854       & High                                         \\ \hline
JMock                                  & Framework                          & 66.40                                                & High Usage                                                    & 9,580         & Low                                          \\ \hline
Metric Miner                           & Framework                          & 71.00                                                & High Usage                                                    & 23,602        & Low                                          \\ \hline
OVal                                   & Framework                          & 75.00                                                & High Usage                                                    & 17,381        & Low                                          \\ \hline
JUnit                                  & Framework                          & 68.00                                                & High Usage                                                    & 25,935        & Low                                          \\ \hline
Lombok                                 & Framework                          & 69.40                                                & High Usage                                                    & 50,324        & Low                                          \\ \hline
Spring Integration                     & Framework                          & 76.70                                                & High Usage                                                    & 208,750       & High                                          \\ \hline
VRaptor                                & Framework                          & 85.00                                                & High Usage                                                    & 26,660        & Low                                          \\ \hline
\end{tabular}
\end{adjustbox}
\end{table}

Based on \cite{Nagappan2013}, we want to determine the similarity and diversity among the selected projects. For this analysis, we propose three dimensions: Type, Percentage of Annotated Classes (PAC), and LOC. Type can assume two values: ``framework'' or ``application''. This dimension is important because it allows capturing two different approaches of annotation usage. From an overall perspective, a framework has its own annotations, and applications use annotations provided by frameworks. PAC, the second dimension, exposes how many classes contain annotations. This allows us to fetch projects ranging from a low annotation usage to heavily based ones. Since this analysis aims to show similarity and diversity, capturing projects with different sizes aids in reaching this goal. Therefore, we include the well-known LOC metric to measure the project's size. With this dimension, this paper can discuss how annotations behave in projects with different sizes. Table \ref{tab:projectDimensions} associates each project with the proposed dimensions.

\begin{table}[hbt]
\centering
\caption{Pair Combination of Dimensions}
\label{tab:pairDimension}
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|l|r|}
\hline
\multicolumn{1}{|c|}{\textbf{Combinations}} & \textbf{Number of  Projects} \\ \hline
Application - Low Annotation                & 5                            \\ \hline
Application - Medium Annotation             & 4                            \\ \hline
Application - High Annotation               & 3                            \\ \hline
Application - Low LOC                       & 4                            \\ \hline
Application - High LOC                      & 8                            \\ \hline
Framework - Low Annotation                  & 3                            \\ \hline
Framework - Medium Annotation               & 3                            \\ \hline
Framework - High Annotation                 & 7                            \\ \hline
Framework - Low LOC                         & 7                            \\ \hline
Framework - High LOC                        & 6                            \\ \hline
Low Annotation - Low LOC                    & 2                            \\ \hline
Low Annotation - High LOC                   & 6                            \\ \hline
Medium Annotation - Low LOC                 & 3                            \\ \hline
Medium Annotation - High LOC                & 4                            \\ \hline
High Annotation - Low LOC                   & 6                            \\ \hline
High Annotation - High LOC                  & 4                            \\ \hline
\textbf{Average}                            & \textbf{4.69}    \\ \hline
\end{tabular}
\end{adjustbox}
\end{table}

For the dimension PAC, we defined three categories: below 35\% - ``low usage'', between 35\% and 65\% - ``medium usage'', and greater than 65\% - ``high usage''. For LOC, we defined two categories: below 100 thousand lines  - ``low'' and above - ``high''. Among the projects, there are 12 frameworks and 13 applications, roughly 50\% for each dimension. Considering projects with high annotations usage, we have 7 projects classified as ``framework'' and 3 projects classified as ``application''. This observation highlights the fact that framework projects are usually more annotation based. From an annotation perspective, the projects have a PAC ranging from 10\% up to 85\%, which shows the diversity. For LOC, there are 11 projects considered ``low'' and 14 considered ``high''. 

Combining the proposed dimensions in pairs, there are a total of 16 possibilities, considering the categories that each dimension can assume. The combinations are Type with PAC, Type with LOC, and PAC with LOC. For the pair Type-PAC, we have ``application'' combined for each of the 3 PAC categories, as well as ``framework'' for the same three categories. This pattern follows on to the total of 16 combinations. For each pair combination, we count the number of projects that fulfill both dimension involved in it. Table \ref{tab:pairDimension} presents the pair combination between dimensions, with the obtained values. There is an average of 4.7 projects per combination, with a minimum value of 2 (``Low PAC - Low LOC'') and a maximum of 8 (``Application - High LOC''). With this analysis we show that, according to our proposed dimensions, we have similar projects and also diverse projects among the chosen 25.


\subsubsection{Data Analysis}
\label{sec:dataAnalysis}

We are interested in analyzing the metrics distribution values among all projects as well as verify if the average value is representative. As already mentioned, some object-oriented metrics follow an exponential distribution graph, which means that the average value does not contribute to understanding the behavior. This paper investigates if annotation metrics also falls into that same category.

In exploratory data analysis (Section~\ref{sec:exploratoryDataAnalysis}), based on an approach proposed by \cite{meirelles2013}, it is verified if the normal distribution is able to fit the data and make a percentile rank empirical analysis to find where the data seems to be meaningful. We use Lanza's approach \citep{Lanza2006} to calculate possible thresholds values based on average and obtained the percentile rank (5\%, 10\%, 25\%, 50\%, 75\%, 90\%, 95\%, 99\%) of each metric to find the threshold where the average is not representative. Afterwards, we confront each method to conclude whether calculating the average value correctly provides useful information.

On the one hand, Lanza's approach is based on a calculation using the average and standard deviation value that was used to define what they called reference regions (Low, Medium, and High). In this paper, we use the following nomenclature:

\begin{itemize}
	\item Lanza-Low = average - standard deviation
	\item Lanza-Medium = average
	\item Lanza-High = average + standard deviation
\end{itemize}

The value of a metric is considered an outlier when it is 50\% greater than the maximum Lanza-High threshold. In summary, \cite{Lanza2006} assume that the average value is meaningful.

On the other hand, adapted from \cite{meirelles2013}, our percentile ranking analysis considers a more realistic approach and yielded in better threshold values. Percentile analysis can be a more flexible way of obtaining the thresholds. As discussed in Section~\ref{sec:exploratoryDataAnalysis}, we use the percentile 90 as a reference point. For instance, \cite{meirelles2013} showed that for object-oriented metrics, the percentile reference is 75. In a normal distribution, we can observe that the median is the reference point because its value is close to the average one. The percentile analyzed in this paper is divided into 3 boundaries, explained below.

\begin{itemize}
	\item Very Frequent = until percentile 90
	\item Frequent = between percentile 90 and 95
	\item Less Frequent = between 95 and 99
\end{itemize}

Using the average and standard deviation (Lanza's approach) is a strict rule, and so it supposes that every metric will behave the same way, which is not true, as we present in Section~\ref{sec:exploratoryDataAnalysis}. Some metrics distributions have an abrupt growth in the final percentiles, in general from the percentile 90. Hence, the percentiles analysis provides a complete visualization of what is happening.
