\section{The Candidate Metrics for Code Annotations}
\label{sec:AnnotatedCodesAssessment}

This section presents the main proposal of this work, which is to evaluate code annotations. To achieve this goal, a metrics suite is defined to assess the quality and complexity of annotated codes, measuring how code annotations are used in the implementation of software. The metrics are classified in this work according to the type of code element used as a basis, which can be the annotation itself, an element (such as a class, method or attribute declaration) or from an overall class perspective. All of the proposed metrics are extracted directly from the source code by using the Annotation Sniffer tool. For this reason, the suite proposed can be considered a set of primary metrics and may allow future metrics to be derived from them \citep{grady1987}.

\estiloJava
\begin{figure}[htb]
%\centering
\begin{minipage}{\linewidth}
\begin{lstlisting}
import javax.persistence.AssociationOverrides;
import javax.persistence.AssociationOverride;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.DiscriminatorColumn;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

@AssociationOverrides(value = {
      @AssociationOverride(name="ex",
         joinColumns = @JoinColumn(name="EX_ID")),
      @AssociationOverride(name="other",
         joinColumns = @JoinColumn(name="O_ID"))})
@NamedQuery(name="findByName",
      query="SELECT c " +
            "FROM Country c " + 
            "WHERE c.name = :name")
@Stateless
public class Example {...

   @TransactionAttribute(SUPPORTS)
   @DiscriminatorColumn(name = "type", 
             discriminatorType = STRING)
   public String exampleMethodA(){...}

   @TransactionAttribute(SUPPORTS)
   public String exampleMethodB(){...}

   @TransactionAttribute(SUPPORTS)
   public String exampleMethodC(){...}

}
\end{lstlisting}
\end{minipage}
\caption{Code for candidate metrics examples.}
\label{fig:metricsexample}
\end{figure}

The source code defined in Figure~\ref{fig:metricsexample} exemplifies how the metrics are extracted. The annotations of such code exist in real-world frameworks. However, their usage mixed in the same class are only for illustrative purposes.

\subsection{Metrics Suite}

We designed a GQM model to guide in the definition of the metrics suite. Our model consists of four questions, which aims to understand how we can assess annotated code. With these questions, we were able to propose seven metrics that, combined, provides enough information to answer the proposed questions from our GQM model presented in Table \ref{tab:gqm}. 

\begin{table}[hbt]
\centering
\caption{GQM approach applied for our annotation metrics proposal.}
\label{tab:gqm}
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{lll}
\hline
\textbf{Goal}   & (Purpose)		& \textit{Assess} \\
         	& (Issue) 		& \textit{the usage of} \\
         	& (Object)	 	& \textit{annotated code} \\
         	& (Viewpoint) 		& \textit{from software developer viewpoint} \\
         	&  			& \\
\hline
(Question) 	& Q1			& \textbf{What is the amount of information defined in an annotation?} \\
\hline
         	&  			& \\
(Metric 1)      & \textbf{AA}		& Attributes in Annotation \\
(Metric 2)     	& \textbf{LOCAD}	& LOC in Annotation Declaration\\
         	&  			& \\
\hline
(Question) 	& Q2 			& \textbf{How hard is to define and maintain an annotation definition?} \\
\hline
         	&  			& \\
(Metric 1)     	& \textbf{AA} 		& Attributes in Annotation\\
(Metric 2)     	& \textbf{LOCAD} 	& LOC in Annotation Declaration\\
(Metric 3)     	& \textbf{ANL}		& Annotation Nesting Level \\
         	&  			& \\
\hline
(Question) 	& Q3  			& \textbf{What is the amount of metadata defined in a class by using annotations?} \\
\hline
         	&  			& \\
(Metric 4)     	& \textbf{AED}		& Annotation in Element Declaration \\
(Metric 5)    	& \textbf{AC}		& Annotations in Class \\
(Metric 6)     	& \textbf{UAC}		& Unique Annotation in Class \\
         	&  			& \\
\hline
(Question) 	& Q4 			& \textbf{How a class is coupled with annotation schemas?} \\
\hline
         	&  			& \\
(Metric 6)     	& \textbf{UAC} 		& Unique Annotation in Class \\
(Metric 7)     	& \textbf{ASC} 		& Annotation Schemas in Class \\
\hline

\end{tabular}
\end{adjustbox}
\end{table}


Alongside the GQM, we grouped the proposed metrics into 3 categories: i) Class metrics; ii) Annotation Metrics; and iii) Code Element Metrics. A class metric measures the annotation from a class perspective, an annotation metric measures the annotation declaration itself and a code element metric measures the annotation on a declared element (it can be a class declaration, a method, or an attribute).

The following subsections define the candidate metrics, explain how each one can help to answer the metrics elaborated in our GQM model, as well as, classifies them and uses Figure~\ref{fig:metricsexample} to illustrate a simple extraction of each of them.

\subsubsection{Attributes in Annotation - AA}

Attributes of an annotation is an important characteristic to be used to provide additional information. A high number of attributes in the same annotation can result in a messy and hard-to-read code. The candidate metric Attributes in Annotation (AA) measures the number of attributes inside an annotation in a code element.

It is possible to define an annotation or a list of annotations as the type of an attribute that goes inside an annotation. In the case of nested annotations, each one is reported separately with their respective attributes number. AA is classified as an Annotation Metric.

Considering the example of Figure~\ref{fig:metricsexample}, the value of AA for the annotation \textsf{@DiscriminatorColumn} is 2, since it has the attributes \textsf{discriminatorType} and \textsf{name}. Another example is \textsf{@AssociationOverrides}, which contains some nested annotations. Its AA value is 1, since there is only 1 attribute (value), while both \textsf{@AssociationOverride} contains 2 attributes each (\textsf{value} and \textsf{joinColumns}); therefore, their AA value is 2.

Based on the GQM from Table \ref{tab:gqm}, AA contribute for the answers of Q1 and Q2. That can be justified by the fact that the number of attributes is directly related to the amount of information present in the annotation, which also reflects the number of parameters that should defined, influencing the code maintenance. This metric can be formally defined by the pseudocode in Algorithm \ref{pseudo_aa}.


\begin{algorithm} 
      \caption{Attributes in Annotation Pseudocode}
      \label{pseudo_aa}
      \begin{algorithmic}
      \Procedure{AA}{$annotation$} 
            \State Initialize $aa = 0$
            \ForEach {$attribute$ $in$ $annotation$}
            \State $aa \gets aa + 1$
            \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}

\subsubsection{Lines of Code in Annotation Declaration - LOCAD}

The types of annotation attributes are limited to primitive types, Strings, enums, instances of a Class, other annotations, or an array of any of these. Due to such limitation, sometimes the information of an attribute can be defined using a large String. Because of that, only the number of attributes defined in an annotation might not be enough to reflect the amount of information defined in an annotation.

The candidate metric LOC (Lines of Code) in Annotation Declaration (LOCAD) measures the number of lines of code used in the source file to define an annotation. LOCAD is classified as an Annotation Metric.

Looking at the code in Figure~\ref{fig:metricsexample}, it is possible to verify that the LOCAD for the annotation \textsf{@NamedQuery} is 4, although the value of AA is only 2.

Similarly to AA, LOCAD aids in Q1 and Q2. Q1 is justified since there is a direct relationship between the number of lines of code and the amount of information in an annotation declaration. Also, the number of lines declared in an annotation influences its maintainability. It is similar to the LOC metric, the more lines a class(or any other code element) contains, the harder it gets to maintain. The pseudocode in Algorithm \ref{pseudo_locad} presents the extraction procedure for LOCAD. Notice the call \texttt{toString}, it returns the lines present in annotation declaration. 

\begin{algorithm} 
      \caption{LOC in Annotation Declaration}
      \label{pseudo_locad}
      \begin{algorithmic}
      \Procedure{LOCAD}{$annotation$} 
            \State Initialize $locad = 0$
            \ForEach {$line$ $in$ $annotation.toString$} 
            \State $locad \gets locad + 1$
            \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}

\subsubsection{Annotation Nesting Level - ANL}

The definition of annotations as attributes of other annotations can help to define a more organized data structure. However, its overuse can lead to annotation definitions that are hard to understand and maintain. The candidate metric Annotation Nesting Level (ANL) measures the maximum level of nesting reached inside an annotation. ANL is classified as an Annotation Metric.

As an example, the annotation \textsf{@AssociationOverrides} in Figure~\ref{fig:metricsexample} has an ANL value of 0. It is easy to figure this out since this annotation is the declaration. Therefore, it is not nested in any other annotation. However, the Annotation AssociationOverride has an ANL of 1, as it is defined inside \textsf{@AssociationOverrides}. Moreover, the Annotation \textsf{@JoinColumn} has ANL value of 2, considering it is defined inside \textsf{@AssociationOverride} which already is the first level of nesting. Notice this metric does not measure the number of attributes in an annotation declaration, while AA does. ANL measures the maximum nesting level of an annotation declaration, which in this example is \textsf{@JoinColumn}.

According to our GQM model, ANL contributes to the Q2. The higher an annotation is nested, the more complexity is involved in its definition, complicating its maintenance. This is similar to a conditional loop (if-else). The deeper a conditional loop gets, the more complicated it gets to maintain it. The pseudocode in Algorithm \ref{pseudo_anl} presents the ANL extraction procedure. The call \texttt{previousCodeElement} returns the type of parent that contains the calling annotation. If it the parent type if an annotation declaration, the call \texttt{isAnnotation} returns \texttt{true}.

\begin{algorithm} 
      \caption{Annotation Nesting Level}
      \label{pseudo_anl}
      \begin{algorithmic}
      \Procedure{ANL}{$annotation$} 
            \State Initialize $anl = 0$
            \If {$annotation.previousCodeElement.isAnnotation$}
            \State $anl \gets 1$ + ANL($annotation.previousCodeElement$)
            \Else
            \State $anl \gets 0$
            \EndIf
      \EndProcedure
      \end{algorithmic}
\end{algorithm}

\subsubsection{Annotations in Element Declaration - AED}

An element in the source code, such as attributes, methods, and classes, may need to have several annotations to inform some metadata. However, an excessive number of annotations in the same element can also reduce code legibility, maintenance, and reusability. The candidate metric Annotations in Element Declaration (AED) focus on each code element individually and measures the number of annotations defined in its context. This metric also counts nested annotations. AED is a Code Element Metric since it focuses on a single element declaration and not the whole class. 

As an example, the value of AED for the method \textsf{exampleMethodA()} in Figure~\ref{fig:metricsexample} is 2 since it has the annotations \textsf{@TransactionAttribute} and \textsf{@DiscriminatorColumn}. A more complicated example is the value of AED for the class \textsf{Example}. Counting the nested annotations the value for this element is 7.

This metric is not concerned with the annotation definition itself, but rather the number of annotations declared in a code element. With our GQM model it contributes to Q3, since it reports the number of metadata configured in a class or code element. The pseudocode in Algorithm \ref{pseudo_aed} exposes the AED extraction procedure. Notice there are two procedures involved. The first one counts how many annotations a code element contains in its declaration. For each annotation, nested annotations must be fetched and accounts for the total AED in a code element, hence the procedure ANNOTATIONS\_IN\_ATTRIBUTES. 

\begin{algorithm} 
      \caption{Annotation in Element Declaration}
      \label{pseudo_aed}
      \begin{algorithmic}
      \Procedure{AED}{$code\_element$} 
            \State Initialize $aed = 0$
            \ForEach {$annotation$ $in$ $code\_element$}
            \State $aed \gets 1$ + ANNOTATIONS\_IN\_ATTRIBUTES($annotation$)
            \EndFor
      \EndProcedure
      \Procedure{ANNOTATIONS\_IN\_ATTRIBUTES}{$annotation$} 
            \State Initialize $aiA = 0$ \Comment Attributes in Annotation
            \ForEach {$attribute$ $in$ $annotation$}
            \If {$attribute.value.isAnnotation$}
            \State $aiA \gets 1$ + ANNOTATIONS\_IN\_ATTRIBUTES($attribute.value$)
            \EndIf
            \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}

\subsubsection{Annotations in Class - AC}

The total number of annotations of a given class can also be important information for the assessment of how annotations are used in its context. The candidate metric Annotations in Class (AC) counts the number of annotations in all elements of a given class including nested annotations. AC is a Class Metric since it being measuring from a class perspective and not from a single element.

Although it can be directly obtained by counting all class annotations, it can also be calculated from the value of AED from all the class elements. The following equation can be used to calculate this metric:

\begin{equation}
AC=\sum_{each \: class \: element} AED
\end{equation}

As an example, the class \textsf{Example} in Figure~\ref{fig:metricsexample} has the value of 11 for the AC metric. All annotations, including the nested ones, are counted. As seen on our GQM model, Q3 is concerned at measuring the number of metadata configured in a class, and the AC metric provides a notion for this.


\begin{algorithm} 
      \caption{Annotation in Class}
      \label{pseudo_ac}
      \begin{algorithmic}
      \Procedure{AC}{$class$} 
      \State Initialize $ac = 0$
      \ForEach {$code\_element$ $in$ $class$}      
      \State $ac \gets ac$ + AED($code\_element$)
      \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}


\subsubsection{Unique Annotations in Class - UAC}

This metric is similar to AC, but it measures the distinct number of annotations in a class. For an annotation to be considered similar to another, they should have the same annotation type and the same value for all of its attributes, which means that at least two code elements are configured with the same metadata. For instance, if two different code elements contain the annotation such as @Annotation1(atr1 = ``This is an example"), then they will be counted only once. They contain the same name (@Annotation1) and their attributes have the same value (atr1 = ``This is an example"). If, for example, the attribute atr1 had different values, they would not be a unique annotation since they are configuring different metadata. In short, the goal of this metric is to register the number of distinct metadata configurations that used annotations in the class. Therefore, UAC is a Class Metric.

Based on the metric values of AC and UAC, it is possible to obtain the number of annotations that are similar to other ones in the same class. A high number of similar annotations can reveal repetition in configurations that might be hard for software maintenance and a high number of distinct annotations means that several particular metadata configurations are performed in the given class.  

The UAC value for the class \textsf{Example} (Figure~\ref{fig:metricsexample}) is 9. The annotations \textsf{@TransactionAttribute} are considered similar as they have the same attribute values, but it is counted only once. However, annotations \textsf{@AssociationOverride} and \textsf{@JoinColumn} are not similar because they have different values for their attributes. Thus, each one is counted separately.

\begin{algorithm} 
      \caption{Unique Annotations in Class}
      \label{pseudo_uac}
      \begin{algorithmic}
      \State define ANNOTATIONS\_LIST
      \Procedure{UAC}{$class$} 
             \State Initialize $uac = 0$
             \ForEach {$code\_element$ $in$ $class$}      
             \ForEach {$annotation$ $in$ $code\_element$}
             \If {!ANNOTATIONS\_LIST$.contains(annotation)$}
             \State $uac \gets 1 +$ UNIQUE\_ANNOTATIONS\_COUNT$(annotation)$
             \State ANNOTATIONS\_LIST$.add(annotation)$
             \EndIf
             \EndFor
             \EndFor
      \EndProcedure
      \Procedure{UNIQUE\_ANNOTATIONS\_COUNT}{$annotation$} 
            \State Initialize $uaC = 0$ \Comment Unique Annotations Counter
            \ForEach {$attribute$ $in$ $annotation$}
            \If {$attribute.value.isAnnotation$ AND\newline
                  !ANNOTATIONS\_LIST.$contains(attribute.value)$}
            \State $uaC \gets 1$ + UNIQUE\_ANNOTATIONS\_COUNT($annotation$)
            \EndIf
            \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}


\subsubsection{Annotation Schemas in Class - ASC}

As stated before, annotation schema can be defined as a set of related annotations that belongs to the same API. For example, the JPA API has an annotation schema with a set of annotations that can be used by applications to define object-relational mapping. When using annotations from different schemas, the code may become tightly coupled with their domains raising reusability problems. Also, it may prevent software evolution as well as compromise its readability and maintenance. The candidate metric ASC (Annotation Schemas in Class) measures the number of annotation schemas that a class is currently using. For the implementation of this metric, we considered that annotations from the same annotation schema belongs to the same package. Therefore, the extraction is simply done by counting the number of distinct annotation packages imported. ASC is a Class Metric. 
On our code example illustrated in Figure~\ref{fig:metricsexample} the ASC is 2. The class is using the EJB schema as well as the JPA schema. Both of these were obtained by directly observing the imported package, and identifying the packages javax.ejb and javax.persistence.


\begin{algorithm} 
      \caption{Annotation Schemas in Class}
      \label{pseudo_asc}
      \begin{algorithmic}
      \State define SCHEMAS\_LIST
      \Procedure{ASC}{$class$} 
      \State Initialize $asc = 0$
      \ForEach {$code\_element$ $in$ $class$}      
      \ForEach {$annotation$ $in$ $code\_element$}
      \If {!SCHEMAS\_LIST.$contains(annotation.schema)$}
      \State $asc \gets asc + 1$
      \State SCHEMAS\_LIST.$add(annotation.schema)$
      \EndIf
      \EndFor
      \EndFor
      \EndProcedure
      \end{algorithmic}
\end{algorithm}

\subsection{Measurements Classification}

The intent of the proposed metrics is to measure certain characteristics about code annotations usage. \cite{basili1996} proposed a generic mathematical framework that defines several concepts, such as size, length, complexity, cohesion, and coupling, used to classify the metrics. The goal of this section is to classify the proposed metrics according to the concepts proposed by \cite{basili1996}, reasoning how they fulfill their required properties.

AA and LOCAD metrics reflect the \textbf{size} properties of an annotation definition: its number of attributes and its lines of code, respectively. These two metrics can be considered similar to measurements that exist for other code elements in regular object-oriented metrics. For instance, it is also possible to measure the number of lines of code and the number of fields (similar to attributes) from a class. AED and AC metrics use the same measurement regarding the number of annotations for a single code element and for the class as a whole, respectively. These four metrics fulfil all the requirements of size metrics, since they cannot be negative (Non-negativity), are null or zero in the absence of annotations (Null value), and can be summed to reflect the size of two elements (Module Additivity).

ANL reflects a \textbf{length} property of an annotation. This classification is justified by the fact that the nesting level of a group of annotations is the greatest nesting level among them, which relates to the Disjoint Modules property. Annotation definitions are static and can always be considered disjoint to each other. The fact that is not possible to add relationships between them make this metric fulfil the properties of Non-Decreasing Monotonicity for Non-Connected Components and Non-Increasing Monotonicity for Connected Components. 

UAC performs a measurement that counts the number of unique annotations. It captures the number of distinct metadata definitions and according to \cite{basili1996} properties can be considered a \textbf{complexity} metric. The unique annotations from a group of classes cannot be less than the UAC from a single class and cannot be greater than the sum of their values, fulfilling the properties of Module Monotonicity and Disjoint Module Additivity.

Finally, ASC reflects a \textbf{coupling} property that characterizes the relationship of a class with annotation schemas. Making an analogy with Afferent Coupling, which count the number of external classes accessed, ASC considers the annotations configured instead of method invocation. It fulfills the properties of coupling metrics since adding new annotations will never decrease its number (Monotonicity), merging two annotation schemas or two classes can only decrease its value (Merging of Modules) and merging unrelated classes will result in a sum of their ASC (Disjoint Module Additivity).


\subsection{Metrics Summary}

We summarize the proposed metrics suite in Table~\ref{tab:metricsSummary}, which has the metric name, its acronym, its reference for measurement, its type based on \citep{basili1996} concepts as well as a brief summary of the metrics definition.

\begin{table}[hbt]
\centering
\caption{Metrics Summary}
\label{tab:metricsSummary}
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{@{}lllll@{}}
\toprule
\textbf{Name}                              & \textbf{Acronym} & \textbf{Reference}    & \textbf{Type}       & \textbf{Summary} \\
\midrule
Attributes in Annotation          & AA      & Annotation   & Size       & \begin{tabular}[c]{@{}l@{}}Measures the number of \\ attributes in an annotation \\ definition\end{tabular}  \\
\hline
LOC in Annotation Declaration     & LOCAD   & Annotation   & Size       & \begin{tabular}[c]{@{}l@{}}Measures the number \\ of lines in an annotation \\ declaration\end{tabular}      \\
\hline
Annotation Nesting Level          & ANL     & Annotation   & Length     & \begin{tabular}[c]{@{}l@{}}Measures the nesting \\ level of an annotation\end{tabular}                       \\ 
\hline
Annotation in Element Declaration & AED     & Code Element & Size       & \begin{tabular}[c]{@{}l@{}}Measures the number \\ of annotations declared \\ on a code element\end{tabular}  \\
\hline
Annotations in Class              & AC      & Class        & Size       & \begin{tabular}[c]{@{}l@{}}Measures the total \\ number of annotations \\ in a class\end{tabular}            \\
\hline
Unique Annotation in Class        & UAC     & Class        & Complexity & \begin{tabular}[c]{@{}l@{}}Measures the number \\ of distinct annotations in \\ a class\end{tabular}          \\
\hline
Annotation Schemas in Class       & ASC     & Class        & Coupling   & \begin{tabular}[c]{@{}l@{}}Measures the number of \\ different annotations \\ schemas in a class \end{tabular} \\
\hline
\end{tabular}
\end{adjustbox}
\end{table}

The metrics in Table~\ref{tab:metricsSummary} refers to 3 different types of code element. The first type, called Annotation, measures information regarding the annotation definition itself, such as the number of attributes. The second type, Code Element, deals with measuring annotations on specific code elements. And finally, there is the Class type, which measures the definition of annotations regarding a whole class, for example, the total number of annotations in a class. 

These metrics will guide this study that aims to understand how annotations are used among projects. It is desired to understand how the metrics distribution will behave, hoping that it will be possible to define some threshold values that might enable the identification of scenarios when annotations are being misused.
