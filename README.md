# R + LaTeX

## Debian Testing

    sudo apt-get install r-recommended r-cran-knitr

## Debian Jessie

    sudo apt-get install r-recommended r-cran-evaluate r-cran-digest r-cran-stringr
    sudo R -e "install.packages('knitr', repos = 'http://www.rforge.net/', type = 'source', dependencies = TRUE)"

## Compilando

    make
