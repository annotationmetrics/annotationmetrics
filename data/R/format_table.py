# You should install pandas library to make it works
import pandas as pd

metrics = ['AA', 'AARE', 'AC', 'ACR', 'AED', 'AMR', 'ANL', 'ARC', 'ARP', 'ARRC',
    'ASC', 'LOC', 'LOCAD', 'NOM', 'UAC']

code = lambda x: '%.2f' % x if isinstance(x, float) else x

for metric in metrics:
    file = '../' + metric + '/' + metric + '.csv'
    print(file)
    file_csv = pd.read_csv(file)
    file_csv = file_csv.applymap(code)
    file_csv.to_csv(file, index=False)
