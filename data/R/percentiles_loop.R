# Load the script that provide us the function to plot percentiles of data
source("percentiles.R")

# You need to set up path variable with the data's directory.
path = "/home/kanashiro/Documents/articles/annotationmetrics/data"

# This array with metrics' name is used as the name of directory that contains
# the data of each metric
metrics = c('AA', 'AARE', 'AC', 'ACR', 'AED', 'AMR', 'ANL', 'ARC', 'ARP',
            'ARRC', 'ASC', 'LOC', 'LOCAD', 'NOM', 'UAC')

# The array with projects' name is used to separete data per project
projects = c("Hibernate_core", "ElasticSearch", "VoltDB", "Spring", "Apache_Tomcat", "Hibernate_commons", "Agilefant", "ANTLR",          "Checkstyle", "Dependometer", "Apache_Derby", "Apache_Isis", "JChemPaint", "Jenkins", "JGit", "JMock", "Junit", "Lombok", "Megamek", "Metric_Miner", "OpenCMS", "Oval", "Apache_Tapestry", "VRaptor", "ArgoUML")

# Function to append a new row in existent data frame
insertRow <- function(existingDF, newrow, row) {
  existingDF[seq(row+1,nrow(existingDF)+1),] <- existingDF[seq(row,nrow(existingDF)),]
  existingDF[row,] <- newrow
  existingDF
}

# Iterate in metrics array
for(i in 1:15){
  metric <- metrics[i]
  print(paste("Processing metric", metric))

  # Set the working directory back to data's home
  setwd(path)

  # Create a directory to put the data
  dir.create(metric)
  
  # Read CSV file with raw data
  data <- read.csv(paste(metric, '.csv', sep=''), header=FALSE)
  #scan(paste(metric, '.csv', sep=''),what=list(NULL),sep='\n',blank.lines.skip = F)
  #print(paste("csv file", data))
  # Now the metric's directory is the working directory
  setwd(metric)
  
  # Iterate in projects array
  for(j in 1:25){
		metric_f = paste(metric,'-',sep='')
    file_name = paste(metric_f, projects[j], sep='') 

    # Call function to plot percentile data
    percentile.plot(dataset = data,
                    position = 'row',
                    index = j,
                    percentiles = c(.05,.1,.25,.5,.75,.9,.95,.99,1),
                    pdf_name = paste(file_name,'.pdf', sep=''),
                    csv_name = paste(file_name,'.csv', sep=''))
  }

  # At this point we have percentile data of each project and each metric.
  # Let's start to join all these data and create a unique CSV file with them.

  # Initialize the data frame with the CSV's headers
  data = data.frame("Projects"=NA, "5%"=NA, "10%"=NA, "25%"=NA, "50%"=NA, "75%"=NA, "90%"=NA, "95%"=NA, "99%"=NA, "100%"=NA, "mean"=NA, "std"=NA)

  # Iterate in projects again, but now we want to join the generated data.
  # Insert a new row in data (data frame).
  for(j in 1:25){
		metric_f = paste(metric,'-',sep='')
    file_name = paste(metric_f, projects[j], sep='') 
    content = read.csv(paste(file_name,'.csv',sep=''), header=FALSE) 
    content_t = t(content)
    #print(content_t)
    content_t <- cbind(c("Project Name",projects[j]), content_t)
    #print(content_t)
    data = insertRow(data, content_t[2,], nrow(data))
  }
  data = data[-nrow(data),]
  
  # Add a collumn with projects' name
  #row.names(data) <- projects

  # sorts the data frame alphabetically
  data <- data[order(data$Projects),]

  # Set the name of collumns
  colnames(data) <- c("Projects", "5%", "10%", "25%", "50%", "75%", "90%", "95%", "99%", "100%", "mean", "std")

 	file_name = paste(metric,'.csv', sep='')

  # Write a new CSV file with these constructed data (format numbers with 2 digits)
  write.csv(format(data, digits=2, nsmall=2), file=file_name, sep=',', quote=FALSE, row.names=FALSE)
}
