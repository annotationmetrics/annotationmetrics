output_fromXMLtoCSV: These folders contains the ouput data after the R script is run (AA, AED...)
    Also, contains the csv files (AA.csv) that served as input to the R script
R folder: Contains the R script necessary to generate de Metrics folder
Metrics.csv (AA.csv, AES.csv....). These files are the input to the R script
raw_output: This folder holds the raw data generated by the XMLtoCSV java program. The xml was generated from the Anifer plugin
processed_output: csv files, after first rearrengment by the fileRearrenger.sh
xml_input: Folder containing the generated XML from ASniffer plugin.