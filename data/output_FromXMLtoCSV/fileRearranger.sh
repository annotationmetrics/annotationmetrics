#!/bin/bash

for i in $(seq 1 25) 
do
    directoryMetric=$(sed -n $i' p;' directoryMetric.txt)
    echo $directoryMetric
    for j in $(seq 1 15)
    do
        finalDirectory=$(sed -n $j' p;' finalDirectory.txt)
        echo $finalDirectory
        #cp /Users/helena/Documents/PaperAnnotations/test/csv_output/$directoryMetric/*_$finalDirectory.csv /Users/helena/Documents/PaperAnnotations/test/data/$finalDirectory
        cp /Users/helena/Documents/PaperAnnotations/annotationmetrics/data/output_FromXMLtoCSV/raw_output/$directoryMetric/*_$finalDirectory.csv /Users/helena/Documents/PaperAnnotations/annotationmetrics/data/output_FromXMLtoCSV/processed_output/$finalDirectory
    done

done

exit

