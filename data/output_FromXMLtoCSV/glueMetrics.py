import os
import csv

#Goes inside the file that contains the name of the metrics
with open('finalDirectory.txt', 'r') as finalDirectory:
    for line in finalDirectory:
        #Now goes inside all files inside the finalDirectory folder
        with open('directoryMetric.txt', 'r') as directoryMetric:
            for line2 in directoryMetric:
                #loop through all csv files
                with open("processed_output/" + line.rstrip('\n') + "/" + line2.rstrip('\n') + "_" + line.rstrip('\n') + ".csv", 'r') as csvMetricFiles:
                    csvFile=open(line.rstrip('\n') + '.csv', 'a')
                    for csvRow in csvMetricFiles:
                        csvFile.write(csvRow + '\n')
                        
            csvFile.close()
